package com.example.exerciserecyclerview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.exerciserecyclerview.adapter.ViewPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val viewPagerAdapter = ViewPagerAdapter(this)
        viewPager2.adapter = viewPagerAdapter
        TabLayoutMediator(tabLayoutBottom, viewPager2) { tab, position ->
            tab.text = "Fragment ${(position + 1)}"
        }.attach()
    }
}
