package com.example.exerciserecyclerview.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.exerciserecyclerview.R
import com.example.exerciserecyclerview.model.User
import kotlinx.android.synthetic.main.item_linear_layout.view.*

class UserAdapter : RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    private var listUser = mutableListOf<User>()

//    @SuppressLint("NotifyDataSetChanged")
//    fun updateUser(listUser: MutableList<User>) {
//        this.listUser = listUser
//        notifyDataSetChanged()
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_linear_layout, parent, true)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount() = listUser.size

    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(position: Int) {
            itemView.imageView.setImageResource(R.drawable.ic_launcher_background)
            itemView.textViewName.text = listUser[position].name
            itemView.textViewAge.text = listUser[position].age
        }
    }

    fun setData(newListUser: MutableList<User>) {
        val diffUtils = UserDiffUtils(listUser, newListUser)
        val diffResult = DiffUtil.calculateDiff(diffUtils)
        listUser = newListUser
        diffResult.dispatchUpdatesTo(this)
    }
}
