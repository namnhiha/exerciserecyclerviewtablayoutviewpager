package com.example.exerciserecyclerview.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.exerciserecyclerview.R
import com.example.exerciserecyclerview.model.User
import kotlinx.android.synthetic.main.item_linear_layout.view.*

class UserListAdapter : ListAdapter<User, UserListAdapter.ViewHolder>(DiffUtils()) {
    private var listUser = mutableListOf<User>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_linear_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(position: Int) {
            itemView.apply {
                textViewName.text = listUser[position].name
                textViewAge.text = listUser[position].age
            }
        }
    }

    fun updateUser(list: MutableList<User>) {
        listUser=list
    }

    fun inserUser(user:MutableList<User>,position: Int)
    {
        listUser.addAll(position,user)
    }

    class DiffUtils : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }
    }
}
