package com.example.exerciserecyclerview.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.exerciserecyclerview.model.User

class UserDiffUtils(
    private var oldList: MutableList<User>,
    private var newList: MutableList<User>
) : DiffUtil.Callback(){

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return when {
            oldList[oldItemPosition].id != newList[oldItemPosition].id -> {
                false
            }
            oldList[oldItemPosition].name != newList[oldItemPosition].name -> {
                false
            }
            oldList[oldItemPosition].age != newList[oldItemPosition].age -> {
                false
            }
            else -> true
        }
    }
}
