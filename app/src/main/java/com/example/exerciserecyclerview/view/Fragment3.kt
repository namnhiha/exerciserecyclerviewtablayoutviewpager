package com.example.exerciserecyclerview.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.exerciserecyclerview.R

class Fragment3 : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("TAG", "onCreateFragment3")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.i("TAG", "onCreateViewFragment3")
        return inflater.inflate(R.layout.fragment_3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i("TAG", "onViewCreatedFragment3")
    }

    override fun onStart() {
        super.onStart()
        Log.i("TAG", "onStartFragment3")
    }

    override fun onResume() {
        super.onResume()
        Log.i("TAG", "onResumeFragment3")
    }

    override fun onPause() {
        super.onPause()
        Log.i("TAG", "onPauseFragment3")
    }

    override fun onStop() {
        super.onStop()
        Log.i("TAG", "onStopFragment3")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("TAG", "onDestroyFragment3")
    }

    companion object {
        @JvmStatic
        fun newInstance() = Fragment3()
    }
}
