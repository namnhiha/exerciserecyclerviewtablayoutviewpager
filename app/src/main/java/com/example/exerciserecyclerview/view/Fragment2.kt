package com.example.exerciserecyclerview.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.exerciserecyclerview.R

class Fragment2 : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("TAG", "onCreateFragment2")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.i("TAG", "onCreateViewFragment2")
        return inflater.inflate(R.layout.fragment_2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i("TAG", "onViewCreatedFragment2")
    }

    override fun onStart() {
        super.onStart()
        Log.i("TAG", "onStartFragment2")
    }

    override fun onResume() {
        super.onResume()
        Log.i("TAG", "onResumeFragment2")
    }

    override fun onPause() {
        super.onPause()
        Log.i("TAG", "onPauseFragment2")
    }

    override fun onStop() {
        super.onStop()
        Log.i("TAG", "onStopFragment2")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("TAG", "onDestroyFragment2")
    }

    companion object {
        @JvmStatic
        fun newInstance() = Fragment2()
    }
}
