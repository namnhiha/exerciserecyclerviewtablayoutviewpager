package com.example.exerciserecyclerview.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exerciserecyclerview.R
import com.example.exerciserecyclerview.adapter.UserListAdapter
import com.example.exerciserecyclerview.model.User
import kotlinx.android.synthetic.main.fragment_1.*

class Fragment1 : Fragment() {
    var list = mutableListOf<User>()
    var list1 = mutableListOf<User>()
    var list2 = mutableListOf<User>()
    val adapter = UserListAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
//        val adapter = UserAdapter()
//        recycleView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
//        recycleView.adapter = adapter
//        adapter.updateUser(list)
//        adapter.setData(list)
        Log.i("TAG", "onCreateFragment1")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.i("TAG", "onCreateViewFragment1")
        return inflater.inflate(R.layout.fragment_1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.updateUser(list)
        recycleView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recycleView.adapter = adapter
        adapter.submitList(list)
        Log.i("TAG", "ViewCreatedFragment1")
    }

    override fun onStart() {
        super.onStart()
        Log.i("TAG", "onStartFragment1")
    }

    override fun onResume() {
        super.onResume()
        buttonList.setOnClickListener {
            list2.addAll(list)
            list2.addAll(1, list1)
            adapter.updateUser(list2)
            adapter.submitList(list2)
        }
        Log.i("TAG", "onResumeFragment1")
    }

    override fun onPause() {
        super.onPause()
        Log.i("TAG", "onPauseFragment1")
    }

    override fun onStop() {
        super.onStop()
        Log.i("TAG", "onStopFragment1")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("TAG", "onDestroyFragment1")
    }

    private fun initData() {
        list.add(User(1, "A", "22"))
        list.add(User(2, "B", "22"))
        list.add(User(3, "C", "22"))
        list.add(User(4, "D", "22"))
        list.add(User(5, "E", "22"))
        list.add(User(6, "F", "22"))
        list.add(User(7, "G", "22"))
        list.add(User(8, "H", "22"))
        list.add(User(9, "I", "22"))
        list.add(User(10, "J", "22"))
        list1.add(User(11, "K", "22"))
        list1.add(User(12, "L", "22"))
    }

    companion object {
        @JvmStatic
        fun newInstance() = Fragment1()
    }
}
